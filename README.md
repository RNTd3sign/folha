# README #

Para rodar o projeto é preciso ter o node e o gulp instalado na máquina.

## Rodando o projeto ##

### Jeito simples ###

* Clique no index.html da pasta public

### Jeito node ###

* No terminal dentro da pasta do projeto

```
	$npm install
	$npm run dev

```


### Observações ###

* Na pasta app, tem a index.html completa e o css compilado do SASS
* Na pasta public, os arquivos minificados.