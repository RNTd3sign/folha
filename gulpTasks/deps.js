const gulp = require('gulp')
const uglify = require('gulp-uglify')
const concat = require('gulp-concat')
const uglifycss = require('gulp-uglifycss')

gulp.task('deps', ['deps.js', 'deps.css'])

gulp.task('deps.js', function(){
    gulp.src([
        'node_modules/jquery/dist/jquery.min.js'
    ])
        .pipe(uglify())
        .pipe(concat('deps.min.js'))
        .pipe(gulp.dest('public/assets/js'))
})

gulp.task('deps.css', function(){
    gulp.src([
        'node_modules/owl.carousel/dist/assets/owl.carousel.min.css'
    ])
        .pipe(uglifycss({ "uglyComments": true}))
        .pipe(concat('deps.min.css'))
        .pipe(gulp.dest('public/assets/css'))

})


